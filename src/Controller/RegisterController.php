<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/inscription", name="inscription")
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function index(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        // Instanciation d'un User
        $user = new User();
        // Instanciation d'un formulaire d'inscripton RegisterType
        $form = $this->createForm(RegisterType::class, $user);
        // Enregistrement d'un nouveau User quand le formulaire est soumis
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            // Cryptage du mot de passe
            $password = $encoder->encodePassword($user, $user->getPassword());
            // Renvoi du mot de passe crypté pour enregistrement
            $user->setPassword($password);
            $this->entityManager->persist($user);
            $this->entityManager->flush();

        }
        return $this->render('register/inscription.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
