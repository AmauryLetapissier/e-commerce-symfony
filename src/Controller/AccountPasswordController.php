<?php

namespace App\Controller;

use App\Form\ChangePasswordType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AccountPasswordController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    /**
     * @Route("/compte/modifier-mon-mot-de-passe", name="account_password")
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function index(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $notification = null;
        //Formulaire pour changement de mot de passe

        // Récupération du User connecté
        $user = $this->getUser();
        // Création du Formulaire basé sur ChangePasswordType
        $form = $this->createForm(ChangePasswordType::class, $user);
        // Gestion du formulaire
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            // Vérifiaction si le mot de passe actuel correspond bien
            $old_password = $form->get('old_password')->getData();
            if($encoder->isPasswordValid($user, $old_password)) {
                // On récupère le nouveau mot de passe
                $new_password = $form->get('new_password')->getData();
                // Cryptage du nouveau mot de passe
                $password = $encoder->encodePassword($user, $new_password);
                $user->setPassword($password);
                // Enregistrement en BDD
                $this->entityManager->flush();
                $notification = 'Votre mot de passe a bien été modifié';
            } else {
                $notification = 'Votre mot de passe actuel n\'est pas le bon';
            }
        }

        return $this->render('account/password.html.twig', [
            'form' => $form->createView(),
            'notification' => $notification
        ]);
    }
}
